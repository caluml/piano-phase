package pianophase;

import javax.sound.midi.*;

import static pianophase.Note.*;

public class Main {

  private static final int END_OF_TRACK = 47;

  /**
   * A Java implementation of <a href="https://en.wikipedia.org/wiki/Piano_Phase">https://en.wikipedia.org/wiki/Piano_Phase</a>
   */
  public static void main(String[] args) throws InvalidMidiDataException, MidiUnavailableException {

    Sequence sequence = new Sequence(Sequence.PPQ, 16);

    int instrument = 1;
    int tempo = 120;

    Track track = sequence.createTrack();  // Begin with a new track

    setInstrument(instrument, track);


    int velocity = 64;   // default to middle volume

    // E4 F♯4 B4 C♯5 D5 F♯4 E4 C♯5 B4 F♯4 D5 C♯5

    int duration = 2;
    int offset = 0;

    for (int i = 0; i < 48; i++) {
      offset = offset + i;
      addStanza(track, velocity, duration, offset);
      offset = offset + (48 - i);
      addStanza(track, velocity, duration, offset);

      offset = offset + i;
      addStanza(track, velocity, duration, offset);
      offset = offset + (48 - i);
      addStanza(track, velocity, duration, offset);

      offset = offset + i;
      addStanza(track, velocity, duration, offset);
      offset = offset + (48 - i);
      addStanza(track, velocity, duration, offset);

      offset = offset + i;
      addStanza(track, velocity, duration, offset);
      offset = offset + (48 - i);
      addStanza(track, velocity, duration, offset);
    }


    Sequencer sequencer = MidiSystem.getSequencer();
    sequencer.open();
    Synthesizer synthesizer = MidiSystem.getSynthesizer();
    synthesizer.open();
    sequencer.getTransmitter().setReceiver(synthesizer.getReceiver());

    // Specify the sequence to play, and the tempo to play it at
    sequencer.setSequence(sequence);
    sequencer.setTempoInBPM(tempo);

    // Let us know when it is done playing
    sequencer.addMetaEventListener(new MetaEventListener() {
      public void meta(MetaMessage m) {
        // A message of this type is automatically sent
        // when we reach the end of the track
        if (m.getType() == END_OF_TRACK) System.exit(0);
      }
    });
    // And start playing now.
    sequencer.start();
  }

  private static void addStanza(Track track, int velocity, int duration, int offset) throws InvalidMidiDataException {
    addNote(track, velocity, E5, 0 + offset, duration);
    addNote(track, velocity, FS5, 4 + offset, duration);
    addNote(track, velocity, B5, 8 + offset, duration);
    addNote(track, velocity, CS6, 12 + offset, duration);
    addNote(track, velocity, D6, 16 + offset, duration);
    addNote(track, velocity, FS5, 20 + offset, duration);
    addNote(track, velocity, E5, 24 + offset, duration);
    addNote(track, velocity, CS6, 28 + offset, duration);
    addNote(track, velocity, B5, 32 + offset, duration);
    addNote(track, velocity, FS5, 36 + offset, duration);
    addNote(track, velocity, D5, 40 + offset, duration);
    addNote(track, velocity, CS6, 44 + offset, duration);
  }

  private static void addNote(Track track, int velocity, Note note, int startTick, int ticks) throws InvalidMidiDataException {
    track.add(new MidiEvent(on(velocity, note.getKey()), startTick));
    track.add(new MidiEvent(off(velocity, note.getKey()), startTick + ticks));
  }

  private static ShortMessage on(int velocity, int key) throws InvalidMidiDataException {
    return message(velocity, key, ShortMessage.NOTE_ON);
  }

  private static ShortMessage off(int velocity, int key) throws InvalidMidiDataException {
    return message(velocity, key, ShortMessage.NOTE_OFF);
  }

  private static ShortMessage message(int velocity, int key, int onOff) throws InvalidMidiDataException {
    ShortMessage message = new ShortMessage();
    message.setMessage(onOff, 0, key, velocity);
    return message;
  }

  private static void setInstrument(int instrument, Track track) throws InvalidMidiDataException {
    ShortMessage message = new ShortMessage();
    message.setMessage(ShortMessage.PROGRAM_CHANGE, 0, instrument, 0);
    track.add(new MidiEvent(message, 0));
  }

}
