package pianophase;

public enum Note {

    C4(48),
    CS4(49),
    D4(50),
    DS4(51),
    E4(52),
    F4(53),
    FS4(54),
    G4(55),
    GS4(56),
    A4(57),
    AS4(58),
    B4(59),

    C5(60),
    CS5(61),
    D5(62),
    DS5(63),
    E5(64),
    F5(65),
    FS5(66),
    G5(67),
    GS5(68),
    A5(69),
    AS5(70),
    B5(71),

    C6(72),
    CS6(73),
    D6(74),
    DS6(75),
    E6(76),
    F6(77),
    FS6(78),
    G6(79),;


    private final int key;

    Note(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}
